
p "hello,world"

a = 1
a = 2
p a  #2が表示
a = "ruby"
p (a + "、world")  #ruby,world

def add( a,b );a + b;end
puts add(1,2)  # a + b →　3

class Hoge
  def test
    puts 1
  end 
  foo = 1
  def bar 
    p "foo"
  end
end

hoge = Hoge.new
hoge.test  #class Hogeのtest メゾットで1表示
hoge.bar   #class Hogeのbar　メゾットで"foo"表示

$foo = 5
def bar2
  p $foo
end
bar2  #グローバル変数メゾットの外側を参照→5 表示
p $bar  # 未初期化なので　nil
