
puts" "
puts "a"  < "b"  # 文字列の比較
puts "ab"  < "ac"
puts "ab" == "ab"
puts "cc" == "aa"

puts ""
foo1 = :"foo1"  # シンボル
puts %s*foo1*
puts %s?foo1?

puts ""
v1 = "foo5"
v2 = v1.to_sym  # v1をシンボルに変換
p v2

puts ""
p "hoge".equal? "hoge"  # equal?→オブジェクトが正しいか
a = ":hoge"
p a.equal? a            # シンボルの時は、true

puts ""
p "foo".eql? "foo"  # eql?→==と同じ結果
p (1.0).eql? 1.0    # fooと同じ、結果

v5 = "foo1"
v6 = v5
p v5.equal? v6  # ture

puts ""
def func v10
  v10.object_id
end

v10 = "foo10"
p v10.object_id  # object_id→オブジェクトごとに固有の整数値
p func(v10)

puts""
destruction = "Destruction"
p destruction_2 = destruction
p destruction.chop  # "Destructio"
p destruction.chop! # "Destructio"

puts ""
make_list = [10,true,"30"]
p make_list [0]
d = [[1,2],[3,4]]
p d	[0][0]

list = Array.new(5)  # array→小文字エラー
p list
p list.length  # 5
p Array.new(2){|box| box + 10}  # [10,11]

creat_list = Array.new(4,"a") # ["a", "a", "a", "a"]
creat_list[0].replace("abc")  # replace→値の変更、入れ替え
p creat_list                  # ["abc", "abc", "abc", "abc"]

create_box = Array.new(4){"a"}  # ブロック({})で初期値を設定
create_box[0].replace("b")
p create_box                    # ["b", "a", "a", "a"]

p create_box[-1]  # 配列の最後から1つ目
p create_box[-4]  # 配列の最後から4つ目

animals = ["dog", "cat", "mouse", "rabbit", "horse"]
p animals[3-1]  # 配列の3番目から引いた番号
animals[1,2] = ["",""]
p animals   # ["dog", "", "", "rabbit", "horse"]


