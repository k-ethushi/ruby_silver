
def bar(a:,b:100,**z)  # **、キーワード引数
  p z                  # 7行目のオブジェクトc,bのシンボルが表示
  a + b
end
p bar(a:2,c:100,d:200) # 102

emerald = 120
if emerald == 100 then
  puts "100円"
  
  elsif emerald >= 150 then
  puts "150円"
  
  else
  puts "120円"
end

a = if false then  # a = if falseにするとnil
  "a"
  elsif true       # a = if falseにすると,判定結果に関わらず、
  "b"                # 変数自体は、確保される
end
p a
                            # unlessは、if文の逆で偽の条件判断
unless emerald != 150 then  # !でif文と同じ意味に
  puts "200円"
  
  else                      # unlessのelsifは、無い 
  puts "emerald"
end


emerald = true ? 120:"jueri"  # 条件演算子 条件式 ? 真の時の値 : 偽の時の値
p emerald

Sapphire = "300円"
print "サファイヤの値段は、#{Sapphire}です。"  # 文字の式展開#{変数名}

b = b || 500  # 変数の初期化していない場合、自己代入
c ||=900
puts c
puts b

p "1"&&"2"  # 2を表示
p "1" and "2"  # 1が表示

def foo
  <<"text"
    Ru
    by
text
end
p foo  # 識別子の終端は、行頭に,識別子とendの間にコメントを入れるとエラー
       # ヒアドキュメント
       # 識別子をダブルとシングルは、文字列と同じ
puts foo

text = %*テキスト*
puts text
p text
