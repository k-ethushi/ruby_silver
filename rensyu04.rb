puts 

a = [2,3,4]
for i in a do
  p a
end

for i,j in [[1,2],[3,4]]
  p i + j  # 1回目,i→1 j→2 2回目,i→3,j→4
end

for i in [2,3,4]
  bar = 1
end
p bar  # forは、ループを抜けた後、参照可
puts 

#[2,3,4].each do
#  scope = 1
#end 
# p scope   eachは、ループを抜けた後、外から参照できない
#                   するとすれば変数に@,$,@@

b = Hash.new(3)
b = {"foo1" =>1,"foo2" =>2,"foo3" =>3}
p b["foo1"]  # 1

c = Hash[:hoofoo1,1,:hoofoo2,2,:hoofoo3,3]
p c

character = "a".."z"
print character
puts 
character.each do |z|
  print z
end

puts
case 1
when 2 then
  p "one"
else
  puts "two"
end

